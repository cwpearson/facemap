#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv/cv.h>

#include <iostream>
#include <cassert>
#include <vector>
#include <algorithm>
#include <string>

const char* cascadeName = "/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml";

using namespace cv;
using namespace std;

// Takes a string like
// "/the/file/path.txt"
// and returns "path.txt"
string getNameFromPath(const string& filePath) {
  string fileName = filePath;
  size_t lastSlash = fileName.find_last_of('/');
  if (lastSlash == string::npos) return fileName;
  else {
    fileName.erase(0, lastSlash + 1);
    return fileName;
  }

}

// Takes an input top left and bottom right corner for the face, and maps it
// down into a smaller Rect for the mustache
Rect faceDimToMustacheDim(const Rect& face) {
  Rect mustache;

  // Determine the dimensions of the face and the mustache zone
  const float faceHeight_f = face.br().y - face.tl().y;
  const float mustacheHeight_f = faceHeight_f / 16.0f;
  const float faceWidth_f = face.br().x - face.tl().x;
  const float mustacheWidth_f = faceWidth_f / 16.0f;

  assert(mustacheHeight_f > 0.0f);
  assert(mustacheWidth_f > 0.0f);

  // The upper-lip is about 1/4 of the way up the face. We have to find the
  // top-left corner of this zone
                               // bottom of face  - 1/4 of height      - half of mustache width
                               //                                      = top of mustache
  const float mustacheTop_f  = float(face.br().y) - faceHeight_f / 3.3 - mustacheHeight_f / 2;
                               // Left of face + 1/2 width          - half of mustache width
                               //                                   = left of mustache
  const float mustacheLeft_f = float(face.tl().x) + faceWidth_f / 2 - mustacheWidth_f / 2;

  assert(mustacheLeft_f > 0.0f);   
  assert(mustacheTop_f > 0.0f);
  assert(mustacheWidth_f > 0.0f);
  assert(mustacheHeight_f > 0.0f); 

  mustache.x = floor(mustacheLeft_f);
  mustache.y = floor(mustacheTop_f);
  mustache.width = ceil(mustacheWidth_f);
  mustache.height = ceil(mustacheHeight_f);

  return mustache;
}

bool vectorIsLarger(const vector<string>& a, const vector<string>& b) {
  return a.size() > b.size();
}

int main( int argc, char* argv[] )
{
  // Print a helpful message if the number of arguments don't make sense
  if (argc == 1) {
    cout << "Usage: ./facemap [one or more image files of the same size]" << endl;
    return -1;
  }

  // Determine the number of stills that we are operating on
  const int numStills = argc - 1;
  
  // Generate an image for the heatmap that is as big as the first still.
  char *filePath = argv[1];
  Mat firstImage = imread(filePath, 1);
  const int stillRows = firstImage.rows;
  const int stillCols = firstImage.cols;

  // One matrix that will be the final heatmap normalized 0-255.
  Mat heatMap;
  heatMap.create(stillRows,stillCols,CV_8U);
  // One matrix that holds the data before normalization
  Mat heatMapData(stillRows, stillCols, CV_32F, 0.0f);

  // Keep a vector of still names that contributed to each pixel in the output map.
  vector<string> *heatMapContributors = new vector<string>[stillRows * stillCols];
 
  // Load the classifier data from the xml file.
  CascadeClassifier haarCascade;
  haarCascade.load(cascadeName);

/*
  FIXME:
  Perhaps change into passes.
  Pass 1: Find the faces in each frame
  Pass 2: Remove face locations that are not present within a certain threshold
          in at least one neighboring frame.
*/
  // Loop through all input files
  float heatMapDataMax = 0.0f;
  for (int stillIndex = 0; stillIndex < numStills; ++stillIndex) {
    filePath = argv[stillIndex + 1]; // Get the filename from input arguments
    Mat image = imread(filePath, 1); // Read the file
    assert(stillRows == image.rows); // make sure its the size we expect
    assert(stillCols == image.cols);

    if (!image.data) {
        cout <<  "Could not open or find the image at ";
        cout << filePath << std::endl ;
        return -1;
    }

    // Convert to grayscale
    Mat grey;
    cvtColor(image, grey, CV_BGR2GRAY);

    // Detect the faces.
    vector<Rect> detection_rois;
    haarCascade.detectMultiScale(grey, detection_rois, 1.2, 5, 
                                 0|CV_HAAR_DO_CANNY_PRUNING);

    // Go through all the detected faces and add them to the heat map
    for (int faceNum = 0; faceNum < detection_rois.size(); ++faceNum) {
      Rect mustache = faceDimToMustacheDim(detection_rois[faceNum]);
      int minCol = mustache.tl().x;
      int maxCol = mustache.br().x;
      int minRow = mustache.tl().y;
      int maxRow = mustache.br().y;
      for (int rowNum = minRow; rowNum < maxRow; ++rowNum) {
        for (int colNum = minCol; colNum < maxCol; ++colNum) {
          // Add the mustache to the map
          heatMapData.at<float>(rowNum, colNum) += 1.0f;
          // Add the current frame to the contributors at that location
          heatMapContributors[rowNum * stillCols + colNum].push_back(getNameFromPath(string(filePath)));

          if (heatMapData.at<float>(rowNum, colNum) > heatMapDataMax) {
            heatMapDataMax += 1.0f;
          }
        }
      }
    }
    cout << detection_rois.size() << " faces in " << filePath << endl;

  } // input file loop

  // FIXME
  if (heatMapDataMax == 0.0f) {
    cerr << "No faces found in any of the images? exiting." << endl;
    return -1;
  }

  // Go through the heat map data and normalize it to 0-255 for black and white display
  for (int x = 0; x < heatMapData.cols; ++x) {
    for (int y = 0; y < heatMapData.rows; ++y) {
      //cerr << "accessing heatMapData" << x << " " << y << endl;
      unsigned char mapval = (heatMapData.at<float>(y,x) * 255.0) / heatMapDataMax;
      //cerr << "accessing heatMap" << endl;
      heatMap.at<unsigned char>(y,x) = mapval;
    }
  }

  // Go through the contributor lists and find the largest ones.
  // Sort the contributor list
  sort(heatMapContributors, heatMapContributors + (stillRows * stillCols), vectorIsLarger);
  // list the first few
  for (int i = 0; i < 20; ++i) {
    for (size_t j = 0; j < heatMapContributors[i].size(); ++j) {
      cout << heatMapContributors[i].operator[](j) << " ";
    }
    cout << endl;
  }
  
  cerr << "Writing image to map.png" << endl;
  imwrite( "map.png", heatMap );
  cerr << "done." << endl;

  //cerr << "Trying to create a window." << endl; 
  //namedWindow( "Display Image", CV_WINDOW_AUTOSIZE );
  //cerr << "Named window done." << endl;
  //imshow( "Heat Map", heatMap );

  delete[] heatMapContributors;

  waitKey(0);

  return 0;
}
