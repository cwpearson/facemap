#! /bin/bash

FACEMAPEXE=/home/pearson/repos/facemap/src/facemap

videofile=`readlink -f "$1"`
videodir=${videofile%\/*}

echo $videofile
echo $videodir

avconv -i "$videofile" -vsync 1 -r 4 -qscale 1 -an -y $videodir/out%d.jpg
echo $videodir
mogrify -path "$videodir" -resize 50% -quality 90 -format jpg "$videodir/*.jpg"

cd $videodir
echo $FACEMAPEXE
$FACEMAPEXE *.jpg
